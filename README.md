

# Pedidos

Resumo da arquitetura definida do projeto conforme solicitado.

### Detalhes da API RESTful
  A API RESTful Backend de pedididos contém as seguintes características: 
   
  Projeto criado com Spring Boot e Java 8.
  
  Banco de dados MySQL com JPA, Hibernate e Spring Data JPA.
  Autenticação e autorização com Spring Security e tokens JWT (JSON Web Token).
  
  Migração de banco de dados com Flyway, banco H2.
  Testes unitários e integração com JUnit e Mockito.
  
  Caching com EhCache.
  Desenvolvimento frontend com Ionic.

### Aplicação hospedada no Heroku

* https://score-spring.herokuapp.com/categorias

### Para realizar o download da aplicação em android, entre no Google Play no link abaixo:

* https://play.google.com/store/apps/details?id=com.relesi.score 

Clique em registrar para criar uma conta.

### Como executar a aplicação

Certifique-se de ter o Maven instalado e adicionado ao PATH de seu sistema operacional, assim como o Git.


git clone https://Relesi@bitbucket.org/Relesi/prevb.git
cd /path/to/your/repo
mvn spring-boot:run

Acesse os endpoints através da url http://localhost:8080

### Importando o projeto no Eclipse, STS ou Intellij
No terminal, execute a seguinte operação:

mvn eclipse:eclipse

No Eclipse/STS, importe o projeto como projeto Maven.

Depois que o projeto estiver importado, mude o application para test e execute os seguintes comandos. 

Maven - update project
mvn clean install
mvn install 

