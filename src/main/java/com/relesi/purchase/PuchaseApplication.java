package com.relesi.purchase;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PuchaseApplication implements CommandLineRunner {
	
	public static void main(String[] args) {
		SpringApplication.run(PuchaseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
