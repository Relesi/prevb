package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.Cliente;

@Component
public class ClienteMother {

	public Cliente getCliente() {

		final Cliente domain = new Cliente();

		domain.setId(null);
		domain.setNome(null);
		domain.setEmail(null);
		domain.setCpfOuCnpj(null);
		domain.getTipo();
		domain.setSenha(null);
		domain.setEnderecos(null);
		domain.getTelefones();
		domain.getPedidos();
		
		return domain;

	}

}
