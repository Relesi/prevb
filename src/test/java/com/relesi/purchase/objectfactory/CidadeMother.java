package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.Cidade;

@Component
public class CidadeMother {

	public Cidade getCidade() {

		final Cidade domain = new Cidade();
		domain.setId(null);
		domain.setNome(null);
		domain.getEstado();

		return domain;
	}

}
