package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.Endereco;

@Component
public class EnderecoMother {

	public Endereco getEndereco() {

		final Endereco domain = new Endereco();

		domain.setId(null);
		domain.setLogradouro(null);
		domain.setNumero(null);
		domain.setComplemento(null);
		domain.setBairro(null);
		domain.setCep(null);
		domain.getCliente();
		domain.getCidade();

		return domain;

	}

}
