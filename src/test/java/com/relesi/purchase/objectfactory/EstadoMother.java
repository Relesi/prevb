package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.Estado;

@Component
public class EstadoMother {

	public Estado getEstado() {

		final Estado domain = new Estado();

		domain.setId(null);
		domain.setNome(null);
		domain.getCidades();

		return domain;
	}

}
