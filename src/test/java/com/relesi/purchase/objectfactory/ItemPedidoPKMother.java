package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.ItemPedidoPK;

@Component
public class ItemPedidoPKMother {

	public ItemPedidoPK getItemPedidoPK() {

		final ItemPedidoPK domain = new ItemPedidoPK();

		domain.getPedido();
		domain.getProduto();

		return domain;
	}

}
