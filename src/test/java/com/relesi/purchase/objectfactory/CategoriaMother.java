package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.Categoria;


@Component
public class CategoriaMother {
	
	
	public Categoria getCategoria() {
		
		final Categoria domain = new Categoria();
		domain.setId(null);
		domain.setNome(null);
		domain.getProdutos();
		
		return domain;
	}
	
	
	
	

}
