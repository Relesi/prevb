package com.relesi.purchase.objectfactory;

import org.springframework.stereotype.Component;

import com.relesi.purchase.domain.ItemPedido;

@Component
public class ItemPedidoMother {

	public ItemPedido getItemPedido() {

		final ItemPedido domain = new ItemPedido();

		domain.getId();
		domain.setDesconto(null);
		domain.setQuantidade(null);
		domain.setPreco(null);

		return domain;
	}

}
