package com.relesi.purchase;


import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.relesi.purchase.PuchaseApplication;


@SpringBootTest(classes = PuchaseApplication.class)
@TestPropertySource(locations="classpath:test.properties")
public class PuchaseApplicationTests {

	@Test
	public void contextLoads() {
	}
	

}
