package com.relesi.purchase.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Categoria;
import com.relesi.purchase.objectfactory.CategoriaMother;
import com.relesi.purchase.repositories.CategoriaRepository;
import com.relesi.purchase.services.CategoriaService;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaServiceTest {

	@InjectMocks
	private CategoriaService service;

	@Mock
	private CategoriaRepository cateRepo;

	@Test
	public void testGetCategoriaById() throws Exception {

		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
//		Categoria i = mother.getCategoria();
//		Mockito.when(cateRepo.findOne(1));
		service.find(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}

	@Test(expected = NullPointerException.class)
	public void testGetCategoriaUpdate() throws Exception {

		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		Mockito.when(cateRepo.findAll());
		service.find(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}

	@Test(expected = NullPointerException.class)
	public void testGetCategoriaInsert() throws Exception {

		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		service.find(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}

	@Test
	public void testGetCategoriaDelete() throws Exception {

		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		service.find(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}
}