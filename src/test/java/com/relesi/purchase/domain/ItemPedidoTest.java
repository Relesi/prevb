package com.relesi.purchase.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.ItemPedidoPK;
import com.relesi.purchase.objectfactory.ItemPedidoPKMother;

@RunWith(MockitoJUnitRunner.class)
public class ItemPedidoTest {

	@Test
	public void testItemPedidoPK() {
		final ItemPedidoPKMother mother = new ItemPedidoPKMother();
		final ItemPedidoPK domain = mother.getItemPedidoPK();
		final ItemPedidoPK response = new ItemPedidoPK();

		Assert.assertEquals("Os valores devem ser iguais", domain.getPedido(), response.getPedido());
		Assert.assertEquals("Os valores devem ser iguais", domain.getProduto(), response.getProduto());

	}

}