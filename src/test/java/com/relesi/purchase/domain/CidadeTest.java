package com.relesi.purchase.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Cidade;
import com.relesi.purchase.objectfactory.CidadeMother;

@RunWith(MockitoJUnitRunner.class)
public class CidadeTest {

	@Test
	public void testCidade() {
		final CidadeMother mother = new CidadeMother();
		final Cidade domain = mother.getCidade();
		final Cidade response = new Cidade();

		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getNome(), response.getNome());
		Assert.assertEquals("Os valores devem ser iguais", domain.getEstado(), response.getEstado());

	}

}
