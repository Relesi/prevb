package com.relesi.purchase.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Cliente;
import com.relesi.purchase.objectfactory.ClienteMother;

@RunWith(MockitoJUnitRunner.class)
public class ClienteTest {

	@Test
	public void testCliente() {
		final ClienteMother mother = new ClienteMother();
		final Cliente domain = mother.getCliente();
		final Cliente response = new Cliente();

		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getNome(), response.getNome());
		Assert.assertEquals("Os valores devem ser iguais", domain.getEmail(), response.getEmail());
		Assert.assertEquals("Os valores devem ser iguais", domain.getCpfOuCnpj(), response.getCpfOuCnpj());
		Assert.assertEquals("Os valores devem ser iguais", domain.getTipo(), response.getTipo());
		Assert.assertEquals("Os valores devem ser iguais", domain.getSenha(), response.getSenha());
		Assert.assertEquals("Os valores devem ser iguais", domain.getTelefones(), response.getTelefones());
		Assert.assertEquals("Os valores devem ser iguais", domain.getPedidos(), response.getPedidos());

	}

}