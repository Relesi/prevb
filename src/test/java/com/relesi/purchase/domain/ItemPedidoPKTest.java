package com.relesi.purchase.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.ItemPedido;
import com.relesi.purchase.objectfactory.ItemPedidoMother;

@RunWith(MockitoJUnitRunner.class)
public class ItemPedidoPKTest {

	@Test
	public void testItemPedido() {
		final ItemPedidoMother mother = new ItemPedidoMother();
		final ItemPedido domain = mother.getItemPedido();
		final ItemPedido response = new ItemPedido();

		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getDesconto(), response.getDesconto());
		Assert.assertEquals("Os valores devem ser iguais", domain.getQuantidade(), response.getQuantidade());
		Assert.assertEquals("Os valores devem ser iguais", domain.getPreco(), response.getPreco());

	}

}