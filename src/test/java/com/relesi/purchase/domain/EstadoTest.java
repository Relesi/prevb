package com.relesi.purchase.domain;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Estado;
import com.relesi.purchase.objectfactory.EstadoMother;

@RunWith(MockitoJUnitRunner.class)
public class EstadoTest {
	
	@Test
	public void testEstado() {
		final EstadoMother mother = new EstadoMother();
		final Estado domain = mother.getEstado();
		final Estado response = new Estado();
		
		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getNome(), response.getNome());
		
		Assert.assertEquals("Os valores devem ser iguais", domain.getCidades(), response.getCidades());

	}

}
