package com.relesi.purchase.domain;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Categoria;
import com.relesi.purchase.objectfactory.CategoriaMother;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaTest {
	
	@Test
	public void testCategoria() {
		final CategoriaMother mother = new CategoriaMother();
		final Categoria domain = mother.getCategoria();
		final Categoria response = new Categoria();
		
		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getNome(), response.getNome());
		Assert.assertEquals("Os valores devem ser iguais", domain.getProdutos(), response.getProdutos());

	}

}
