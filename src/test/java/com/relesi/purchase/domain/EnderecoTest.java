package com.relesi.purchase.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.relesi.purchase.domain.Endereco;
import com.relesi.purchase.objectfactory.EnderecoMother;

@RunWith(MockitoJUnitRunner.class)
public class EnderecoTest {

	@Test
	public void testEndereco() {
		final EnderecoMother mother = new EnderecoMother();
		final Endereco domain = mother.getEndereco();
		final Endereco response = new Endereco();

		Assert.assertEquals("Os valores devem ser iguais", domain.getId(), response.getId());
		Assert.assertEquals("Os valores devem ser iguais", domain.getLogradouro(), response.getLogradouro());
		Assert.assertEquals("Os valores devem ser iguais", domain.getNumero(), response.getNumero());
		Assert.assertEquals("Os valores devem ser iguais", domain.getComplemento(), response.getComplemento());
		Assert.assertEquals("Os valores devem ser iguais", domain.getBairro(), response.getBairro());
		Assert.assertEquals("Os valores devem ser iguais", domain.getCep(), response.getCep());
		Assert.assertEquals("Os valores devem ser iguais", domain.getCliente(), response.getCliente());
		Assert.assertEquals("Os valores devem ser iguais", domain.getCidade(), response.getCidade());

	}
}
