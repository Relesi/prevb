package com.relesi.purchase.resources;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import com.relesi.purchase.domain.Cliente;
import com.relesi.purchase.dto.ClienteDTO;
import com.relesi.purchase.objectfactory.ClienteMother;
import com.relesi.purchase.services.ClienteService;

@RunWith(MockitoJUnitRunner.class)
public class CLienteTest {

	@InjectMocks
	private ClienteResource resource;

	@Mock
	private ClienteService service;

	@Test
	public void testGetById() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		final ResponseEntity<Cliente> result = resource.find(0);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);

	}

	@Test
	public void testGetByEmail() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		final ResponseEntity<Cliente> result = resource.find("rena@Re.com");
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);

	}

	@Test
	public void testInsert() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		Mockito.when(this.service.insert(mock)).thenReturn(mock);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}

	@Test(expected = NullPointerException.class)
	public void testUpdate() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		Mockito.when(this.service.find(null)).thenReturn(mock);
		final ResponseEntity<Void> result = resource.update(null, null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

	@Test
	public void testDelete() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		final ResponseEntity<Void> result = resource.delete(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

	@Test(expected = NullPointerException.class)
	public void testfindPage() {
		ClienteMother mother = new ClienteMother();
		final Cliente mock = mother.getCliente();
		final ResponseEntity<Page<ClienteDTO>> result = resource.findPage(null, null, null, null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

}
