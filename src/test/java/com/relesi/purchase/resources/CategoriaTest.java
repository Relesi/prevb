package com.relesi.purchase.resources;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import com.relesi.purchase.domain.Categoria;
import com.relesi.purchase.dto.CategoriaDTO;
import com.relesi.purchase.objectfactory.CategoriaMother;
import com.relesi.purchase.services.CategoriaService;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaTest {

	@InjectMocks
	private CategoriaResource resource;

	@Mock
	private CategoriaService service;

	@Test
	public void testGetById() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		Mockito.when(this.service.find(null)).thenReturn(mock);
		final ResponseEntity<Categoria> result = resource.find(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);

	}

	@Test(expected = NullPointerException.class)
	public void testInsert() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		Mockito.when(this.service.insert(null)).thenReturn(mock);
		Assert.assertEquals("Aqui o status code deverá ser 200", mock, mock);

	}

	@Test(expected = NullPointerException.class)
	public void testUpdate() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		Mockito.when(this.service.update(mock)).thenReturn(mock);
		final ResponseEntity<Void> result = resource.update(null, null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

	@Test
	public void testDelete() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		final ResponseEntity<Void> result = resource.delete(null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

	@Test(expected = NullPointerException.class)
	public void testFindAll() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		final ResponseEntity<List<CategoriaDTO>> result = resource.findAll();
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);

	}

	@Test(expected = NullPointerException.class)
	public void testFindPage() {
		CategoriaMother mother = new CategoriaMother();
		final Categoria mock = mother.getCategoria();
		final ResponseEntity<Page<CategoriaDTO>> result = resource.findPage(null, null, null, null);
		Assert.assertEquals("Aqui o status code deverá ser 200", result, result);
		Assert.assertEquals("Aqui a lista deverá ser igual", result.getBody(), mock);

	}

}
